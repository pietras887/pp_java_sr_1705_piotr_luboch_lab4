//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BMIGUI
{
	private int WIDTH = 300;
	private int HEIGHT = 120;

	private JFrame frame;
	private JPanel panel;
	private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel;
	private JTextField wzrost, waga;
	private JButton oblicz;

	// -----------------------------------------------------------------
	// Ustawia GUI.
	// -----------------------------------------------------------------
	public BMIGUI()
	{
		frame = new JFrame("Kalkulator BMI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// tworzenie etykiety dla pol tekstowych wzrostu i wagi
		wzrostLabel = new JLabel("Twoj wzrost w metrach:");
		wagaLabel = new JLabel("Twoja waga w kilokramach: ");

		// stworz etykiete "to jsest twoje BMI"
		BMILabel = new JLabel("to jest twoje BMI");
		// stworz etykiete wynik dla wartosci BMI
		wynikLabel = new JLabel("wynik");
		// stworz JTextField dla wzrostu
		wzrost = new JTextField(10);
		// stworz JTextField dla wagi
		waga = new JTextField(10);
		// stworz przycisk, ktory po wcisnieciu policzy BMI
		oblicz = new JButton("Oblicz BMI");
		// stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal
		// nacisniety
		oblicz.addActionListener(new BMIListener());
		// ustawienia JPanel znajdujacego sie na JFrame
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		panel.setBackground(Color.yellow);

		// dodaj do panelu etykiete i pole tekstowe dla wzrostu
		panel.add(wzrostLabel);
		panel.add(wzrost);
		// dodaj do panelu etykiete i pole tekstowe dla wagi
		panel.add(wagaLabel);
		panel.add(waga);
		// dodaj do panelu przycisk
		panel.add(oblicz);
		// dodaj do panelu etykiete BMI
		panel.add(BMILabel);
		// dodaj do panelu etykiete dla wyniku
		panel.add(wynikLabel);
		// dodaj panel do frame
		frame.getContentPane().add(panel);
	}

	// -----------------------------------------------------------------
	// Wyswietl frame aplikacji podstawowej
	// -----------------------------------------------------------------
	public void display()
	{
		frame.pack();
		frame.show();
	}

	// *****************************************************************
	// Reprezentuje action listenera dla przycisku oblicz.
	// *****************************************************************
	private class BMIListener implements ActionListener
	{
		// --------------------------------------------------------------
		// Wyznacz BMI po wcisnieciu przycisku
		// --------------------------------------------------------------
		public void actionPerformed(ActionEvent event)
		{
			String wzrostText, wagaText;
			double wzrostVal, wagaVal;
			double bmi;

			// pobierz tekst z pol tekstowych dla wagi i wzrostu
			wzrostText = wzrost.getText();
			wagaText = waga.getText();
			// Wykorzystaj Integer.parseInt do konwersji tekstu na integer
			wzrostVal = Double.parseDouble(wzrostText);
			wagaVal = Double.parseDouble(wagaText);
			// oblicz bmi = waga / (wzrost)^2
			bmi = wagaVal / (wzrostVal * wzrostVal);
			// Dodaj wynik do etykiety dla wyniku.
			// Wykorzystaj Double.toString do konwersji double na string.
			wynikLabel.setText(Double.toString(bmi)+BMI.ocenBMI(bmi));
		}
	}
}
