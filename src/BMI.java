//********************************************************************
//  BMI.java   
//
//  Wykorzystuje GUI do wyznaczania wspolczynnika BMI.
//********************************************************************

public class BMI
{
  
   public static void main (String[] args)
   {

       //Stworz nowy obiekt BMIGUI
	   BMIGUI bmi = new BMIGUI();
       //wywolaj metode wyswietlajaca obiekt BMIGUI
	   bmi.display();
   }
   
   public static String ocenBMI(double bmi)
   {
	  if(bmi<19)
		  return "niedowaga";
	  if(bmi<=25)
		  return "waga prawid�owa";
	  if(bmi<=30)
		  return "nadwaga";
	  else
		  return "oty�o��";
   }
}

